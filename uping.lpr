program uping;
{$mode objfpc}{$H+}
uses sysutils, sockets, BaseUnix, errors, cnetdb;

const
  PING_PORT = 7464;
  PING_TTL = 3;

type
  PPongRec = ^TPongRec;
  TPongRec = array[1..255] of Byte;
  TPingBuf = packed record
    seq: Integer;
    payload: array[0..8191] of Byte;
  end;

procedure log(msg: string);
begin
  WriteLn('[', FormatDateTime('yyyy-mm-dd hh:nn:ss', Now), '] ', msg);
end;

procedure ping(fd: Integer; dst: psockaddr; mtu: Integer; sn: Integer);
var
  buf: TPingBuf;
  w: Integer;
begin
  buf.seq := sn;
  w := fpsendto(fd, @buf, mtu, 0, dst, SizeOf(sockaddr));
  if w < 0 then log('[ping] fpsendto: ' + StrError(errno))
  else if w <> mtu then
    log('[ping] len=' + IntToStr(mtu) + ', written=' + IntToStr(w));
end;

function pong(fd: Integer; map: PPongRec): Integer;
var
  buf: TPingBuf;
  sl: TSockLen;
  src: sockaddr;
begin
  sl := SizeOf(sockaddr);
  if fprecvfrom(fd, @buf, SizeOf(buf), 0, @src, @sl) < 0 then
    log('[pong] fpread: ' + StrError(errno))
  else begin
    Result := buf.seq;
    if Result > 0 then map^[Result] := 1;
  end;
end;

procedure run_server(port: Word);
var
  addr: sockaddr_in;
  fd, cnt, v: Integer;
  started: Boolean;
  ts: TDateTime;
  map: TPongRec;
  pid: Integer;
begin
  FillChar(addr{%H-}, SizeOf(addr), 0);
  addr.sin_family := AF_INET;
  addr.sin_addr.s_addr := htons(INADDR_ANY);
  addr.sin_port := htons(port);
  fd := fpsocket(AF_INET, SOCK_DGRAM, 0);
  if fd = -1 then begin
    log('[run_server] fpsocket: ' + StrError(errno));
    Exit;
  end;
  if fpbind(fd, @addr, SizeOf(addr)) = -1 then begin
    log('[run_server] fpbind: ' + StrError(errno));
    Exit;
  end;
  started := False;
  ts := 0;
  while True do begin
    pid := pong(fd, @map);
    if pid <= 0 then begin
      if not started then begin
        FillByte(map, SizeOf(map), 0);
        ts := 0;
      end else if ts = 0 then begin
        ts := Now;
      end else if (Now - ts) * 86400 > PING_TTL then begin
        cnt := 0;
        for v := 1 to 255 do if map[v] <> 0 then Inc(cnt);
        log(Format('%d/255 received (packet loss %0.1f%%)', [cnt, 100-cnt/2.55]));
        started := False;
      end;
    end else started := True;
  end;
end;

procedure run_client(server: string; port, mtu: Word);
var
  svr: sockaddr;
  host: PHostEnt;
  fd, i: Integer;
  ra: in_addr;
begin
  FillChar(svr{%H-}, SizeOf(svr), 0);
  fd := fpsocket(AF_INET, SOCK_DGRAM, 0);
  if fd = -1 then begin
    log('[run_client] fpsocket: ' + StrError(errno));
    Exit;
  end;
  ra := StrToNetAddr(server);
  if ra.s_addr = 0 then begin //is domain name
    host := gethostbyname(PChar(server));
    if not Assigned(host) then begin
      LOG('[run_client] gethostbyname: cannot resolve ' + server);
      Exit;
    end;
    ra.s_addr := PCardinal(host^.h_addr_list[0])^;
    server := NetAddrToStr(ra);
  end;
  log('UPinging [' + server + '], CTRL-C to break');
  svr.sin_family := AF_INET;
  svr.sin_port := htons(port);
  svr.sin_addr := ra;
  while True do begin
    for i := -255 to 0 do ping(fd, @svr, mtu, i);
    for i := 1 to 255 do ping(fd, @svr, mtu, i);
  end;
end;

begin
  if ParamCount = 0 then run_server(PING_PORT)
  else                   run_client(ParamStr(1), PING_PORT, 1400);
end.

